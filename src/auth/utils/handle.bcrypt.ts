import * as bcrypt from 'bcryptjs';
const saltOrRounds = 10;

async function generateHash(password: string): Promise<string> {
  const hash = await bcrypt.hash(password, saltOrRounds);
  return hash;
}

async function compareHash(password: string, hash: string): Promise<boolean> {
  return await bcrypt.compare(password, hash);
}

export { generateHash, compareHash };
