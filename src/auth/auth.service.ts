import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/users/schema/users.shema';
import { LoginAuthDto } from './dto/login-auth.dto';
import { RegisterAuthDto } from './dto/register-auth.dto';
import { compareHash, generateHash } from './utils/handle.bcrypt';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly eventEmitter: EventEmitter2,
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  /**
   * Autenticar usuario
   * @param loginAuthDto
   * @returns
   */
  public async login(loginAuthDto: LoginAuthDto) {
    const { password } = loginAuthDto;
    const userSelect = await this.userModel.findOne({
      email: loginAuthDto.email,
    });
    if (!userSelect) throw new HttpException('NOT FOUND', HttpStatus.NOT_FOUND);
    const isCheck = await compareHash(password, userSelect.password);
    if (!isCheck)
      throw new HttpException('CONTRASEÑA INCORRECTA', HttpStatus.CONFLICT);

    // Primero convertimos a un object para eliminar un propiedad.
    const userFlat = userSelect.toObject();
    // Eliminamos la propiedad.
    delete userFlat.password;
    // Payload.
    const Payload = {
      id: userFlat._id,
    };
    // Creacion del JWT
    const token = this.jwtService.sign(Payload);
    // Creamos el objecto a retornar.
    const data = {
      token: token,
      user: userFlat,
    };
    // Retornamos el usuario.
    return data;
  }

  /**
   * Registrar usuario.
   * @param registerAuthDto
   * @returns
   */
  public async register(registerAuthDto: RegisterAuthDto) {
    const { password, ...user } = registerAuthDto;
    const userParse = { ...user, password: await generateHash(password) };

    const newUser = await this.userModel.create(userParse);

    // * [Modulo] Send Email ...
    this.eventEmitter.emit('user.created', newUser);

    return newUser;
  }
}
