import { ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { json } from 'express';
import { AppModule } from './app.module';

async function bootstrap() {
  // TODO: Iniciamos la app con CORS.
  const app = await NestFactory.create(AppModule, { cors: true });

  // Maximo de peso de la informacion enviada.
  app.use(json({ limit: '60mb' }));

  // Versionamiento del API.
  app.enableVersioning({
    defaultVersion: '1',
    type: VersioningType.URI,
  });

  // TODO: Activamos las validacion de manera global.
  app.useGlobalPipes(new ValidationPipe());

  // TODO: Documentacion con Swagger.
  const config = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('API NestJs - Courses')
    .setDescription('This is the official documentation of my API.')
    .setVersion('1.0')
    .addTag('Info')
    .addTag('Auth')
    .addTag('Courses')
    .addTag('Videos')
    .addTag('Awards')
    .addTag('Users')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('documentation', app, document);

  // Asignamos el puerto.
  console.log('__ENV__', process.env.PORT);
  await app.listen(3000);
}
bootstrap();
