import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const PaginateV2 = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request: any = ctx.switchToHttp().getRequest();
    const pagination = request.paginate;
    return pagination;
  },
);
