import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseInterceptors,
  UploadedFile,
  HttpCode,
} from '@nestjs/common';
import { VideosService } from './videos.service';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { ApiTags } from '@nestjs/swagger';
import { LoggerInterceptor } from 'src/utils/logger.interceptor';
import { FileInterceptor } from '@nestjs/platform-express';
import { storage } from 'src/utils/media.hundle';
import { Rol } from '../decorators/rol.decorator';

@ApiTags('Videos')
@UseInterceptors(LoggerInterceptor)
@Controller('videos')
// @UsePipes(new ValidationPipe())-> Validacion local.
export class VideosController {
  constructor(private readonly videosService: VideosService) {}

  @Post()
  create(@Body() createVideoDto: CreateVideoDto) {
    console.table(createVideoDto);
    return this.videosService.create(createVideoDto);
  }

  @Post('upload/:id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  @UseInterceptors(FileInterceptor('avatar', { storage }))
  uploadFile(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(file);
    return this.videosService.addVideo(id, file.filename);
  }

  @Get()
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  findAll(@Query() query: string) {
    // Quey Params.
    console.log(query);
    return this.videosService.findAll();
  }

  @Get(':id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  findOne(@Param('id') id: string) {
    return this.videosService.findOne(+id);
  }

  @Patch(':id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  update(@Param('id') id: string, @Body() updateVideoDto: UpdateVideoDto) {
    return this.videosService.update(+id, updateVideoDto);
  }

  @Delete(':id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  remove(@Param('id') id: string) {
    return this.videosService.remove(+id);
  }
}
