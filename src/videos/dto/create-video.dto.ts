import { IsNotEmpty, IsUrl, IsUUID, Length } from 'class-validator';

export class CreateVideoDto {
  @IsNotEmpty()
  @Length(1, 50)
  title: string;

  @IsNotEmpty()
  @Length(1, 150)
  description: string;

  @IsNotEmpty()
  @IsUUID()
  idCourse: string;
}
