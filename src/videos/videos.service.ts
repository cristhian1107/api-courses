import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { Video, VideoDocument } from './schema/videos.schema';

interface ModelExt<T> extends Model<T> {
  delete: Function;
  findAllCourses: Function;
}

@Injectable()
export class VideosService {
  constructor(
    @InjectModel(Video.name)
    private readonly videoModel: ModelExt<VideoDocument>,
  ) {}

  async create(createVideoDto: CreateVideoDto) {
    try {
      const result = await this.videoModel.create(createVideoDto);
      return result;
    } catch (e) {
      console.log('__ERROR__:', e);
      throw e;
    }
  }

  async addVideo(id: string, filename: string) {
    try {
      const result = await this.videoModel.findOneAndUpdate(
        { id },
        { source: filename },
        {
          upsert: true,
          new: true,
        },
      );
      return result;
    } catch (e) {
      console.log('__ERROR__:', e);
      throw e;
    }
  }

  findAll() {
    return `This action returns all videos`;
  }

  findOne(id: number) {
    return `This action returns a #${id} video`;
  }

  update(id: number, updateVideoDto: UpdateVideoDto) {
    return `This action updates a #${id} video`;
  }

  remove(id: number) {
    return `This action removes a #${id} video`;
  }
}
