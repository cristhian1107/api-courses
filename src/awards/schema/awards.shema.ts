import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export type AwardDocument = Award & Document;

@Schema({ timestamps: true })
export class Award {
  @Prop({ unique: true, default: uuidv4 })
  id: string;

  @Prop({ required: true })
  title: string;

  @Prop()
  idUser: mongoose.Types.ObjectId;

  @Prop()
  description: string;
}

export const AwardSchema = SchemaFactory.createForClass(Award);
