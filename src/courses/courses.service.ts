import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { Course, CourseDocument } from './schema/courses.scheme';
import { Model, Types } from 'mongoose';

interface ModelExt<T> extends Model<T> {
  delete: Function;
  paginate: Function;
  findAllCourses: Function;
}

@Injectable()
export class CoursesService {
  constructor(
    @InjectModel(Course.name)
    private readonly courseModel: ModelExt<CourseDocument>,
  ) {}

  async create(createCourseDto: CreateCourseDto) {
    try {
      const result = await this.courseModel.create(createCourseDto);
      return result;
    } catch (e) {
      console.log('__ERROR__:', e);
      throw e;
    }
  }

  async findAll(pagination: any) {
    try {
      // * Tradicional
      // const list = await this.courseModel.find({});
      // * Join
      //const list = await this.courseModel.findAllCourses();
      // * Paginación
      // const options = {
      //   page: 2,
      //   limit: 5,
      //   // collation: {
      //   //   locale: 'es',
      //   // },
      // };
      const list = await this.courseModel.paginate({}, pagination);
      return list;
    } catch (e) {
      throw e;
    }
  }

  async findOne(id: string) {
    return await this.courseModel.findOne({ id }, { _id: 0 }).exec();
  }

  async update(id: string, updateCourseDto: UpdateCourseDto) {
    return await this.courseModel.findOneAndUpdate({ id }, updateCourseDto, {
      upsert: true,
      new: true,
    });
  }

  async remove(id: string) {
    const _id = new Types.ObjectId(id);
    const response = await this.courseModel.delete({ _id });
    return response;
  }
}
