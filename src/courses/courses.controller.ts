import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpException,
  HttpStatus,
  UseGuards,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { BrowserAgentGuard } from 'src/guards/browser-agent.guard';
import { JwtGuard } from 'src/guards/jwt.guard';
import { PaginateV2 } from '../decorators/paginate-v2.decorator';
import { Rol } from '../decorators/rol.decorator';
import { RolesGuard } from '../guards/roles.guard';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { SlugPipe } from './pipes/slug.pipe';

@ApiBearerAuth()
@ApiTags('Courses')
// !@UseGuards(JwtGuard, BrowserAgentGuard, new RolesGuard('admin'))
@UseGuards(JwtGuard, BrowserAgentGuard, RolesGuard)
@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @Post()
  @HttpCode(201) // Created.
  @Rol(['admin'])
  create(@Req() req: Request, @Body() createCourseDto: CreateCourseDto) {
    try {
      // *console.log(req.user);
      return this.coursesService.create(createCourseDto);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Get()
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  findAll(@PaginateV2() pagination: any) {
    console.log('___PAGINATION___', pagination);
    return this.coursesService.findAll(pagination);
  }

  // @Get(':title')
  // @Rol(['manager', 'admin'])
  // findOne(
  //   @Param('title', new SlugPipe())
  //   title: string,
  // ) {
  //   try {
  //     console.log('___TITLE___', title);
  //     return this.coursesService.findOne(1);
  //   } catch (e) {
  //     throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
  //   }
  // }

  @Get(':id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  findOne(@Param('id') id: string) {
    console.log(id);
    return this.coursesService.findOne(id);
  }

  @Patch(':id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  update(@Param('id') id: string, @Body() updateCourseDto: UpdateCourseDto) {
    return this.coursesService.update(id, updateCourseDto);
  }

  @Delete(':id')
  @HttpCode(200)
  @Rol(['admin', 'user', 'manager'])
  remove(@Param('id') id: string) {
    return this.coursesService.remove(id);
  }
}
