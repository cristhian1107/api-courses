import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';

// TODO: Esto nos sirve para validar informacion previa del contexto!!!.
@Injectable()
export class BrowserAgentGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // TODO: En nuestro caso el contexto es HTTP.
    // Obtenemos el request ->[0], response ->[1].
    const req = context.getArgByIndex(0);
    // Obtenemos encabezados y podemos validarlos.
    const userAgent = req.headers['user-agent'];
    console.log(userAgent);
    // Ejemplo
    const isAllowed = userAgent != 'BrowserAgent';
    // Si queremos personalizar el por defaulr 403->Forbidden.
    if (!isAllowed)
      throw new HttpException('Browser Agent Invalid', HttpStatus.UNAUTHORIZED);
    // Retornamos la validacion.
    return isAllowed;
  }
}
