import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class RolesGuard implements CanActivate {
  // * Manual.
  // !constructor(private rol: string) {}

  // * Metadata.
  constructor(private readonly reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // TODO: En nuestro caso el contexto es HTTP.
    // Obtenemos el request ->[0], response ->[1].
    const req = context.getArgByIndex(0);

    const { roles } = req.user;
    // * Manual.
    // !const isAllow = roles.includes(this.rol);
    // !const isAllow = roles.includes(getRolMeta);
    // * Metadata.
    const getRolMeta = this.reflector.get<string[]>(
      'rol',
      context.getHandler(),
    );
    console.log('__ROLE__: ', getRolMeta);
    const isAllow = roles.some((rol) => getRolMeta.includes(rol));
    return isAllow;
  }
}
