import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getInfo(): string {
    return `<h1>RestApi Courses</h1>
    <h2>Description</h2>
    <p> Simple Rest API using NestJs, TypeScript, MongoDB that lists content about courses.<p>
    <h2>Repository</h2>
    <p>Visit! <a href="https://gitlab.com/cristhian1107/api-courses" target="_blank">GitLab API Courses</a></p>
    <h2>Author</h2>
    <p><a href="mailto:cristhian.cjaa@gmail.com" target="_blank">Cristhian Apaza</a></p>`;
  }
}
