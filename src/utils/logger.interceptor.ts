import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // TODO: En nuestro caso el contexto es HTTP.
    // Sabiendo esto podemos hacer ...

    // Controlar entrada.
    const [req] = context.getArgs();
    console.log('Before ...', req.params);
    // Controlar salida.
    return next.handle().pipe(tap((value) => console.log('After ...', value)));
  }
}
