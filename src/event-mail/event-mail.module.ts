import { Module } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { UserDocument } from '../users/schema/users.shema';
import { MailerService } from '@nestjs-modules/mailer/dist';

@Module({})
export class EventMailModule {
  constructor(private readonly mailService: MailerService) {}

  @OnEvent('user.created')
  handleOrderCreatedEvent(user: UserDocument) {
    console.log('__EVENT_EMAIL__', user);
    this.mailService.sendMail({
      to: user.email,
      bcc: 'cristhian.cjaa@outlook.com',
      subject: 'Bienvenido a tu app NestJS',
      template: 'welcome',
      context: {
        name: user.name,
      },
    });
    // handle and process "OrderCreatedEvent" event
  }
}
