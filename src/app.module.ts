import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoursesModule } from './courses/courses.module';
import { AuthModule } from './auth/auth.module';
import { VideosModule } from './videos/videos.module';
import { AwardsModule } from './awards/awards.module';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { EventMailModule } from './event-mail/event-mail.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { MailModule } from './mail/mail.module';

@Module({
  imports: [
    // Variables de entorno.
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    // Servidor estatico - index.html
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    // Configure Events.
    EventEmitterModule.forRoot(),
    // MongoDB Connection y pluggis.
    MongooseModule.forRoot(process.env.DB_URI, {
      connectionFactory: (connection) => {
        const pluginOption = { overrideMethods: 'all' };
        connection.plugin(require('mongoose-delete'), pluginOption);
        return connection;
      },
    }),
    CoursesModule,
    AuthModule,
    VideosModule,
    AwardsModule,
    UsersModule,
    EventMailModule,
    MailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
